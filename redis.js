const redis = require('redis'),
    // client = redis.createClient('redis://redis:6379');
    client = redis.createClient();

client.on('connect', () => {
    console.log('Redis connected')
});

// echo redis errors to the console
client.on('error', (err) => {
    console.log("Error " + err)
});

const express = require('express');

const app = express();
const log = console;

var midWare = (req, res, next) => {
    // let data = JSON.stringify(req.query);
    // let buff = new Buffer(data);
    // let base64data = buff.toString('base64');
    const key = req.originalUrl;
    // console.log(req);
    if ('cache-control' in req.headers && req.headers['cache-control'] == 'no-cache') {
        client.flushdb(function (err, succeeded) {
            console.log(succeeded); // will be true if successfull
        });
    }
    client.get(key, (err, result) => {
        // console.log(key);
        console.log(result);
        if (err == null && result != null) {
            // res.status(304);
            console.log('cached');
            // console.log(result);
            // res.set('cached', true);
            res.send(result);
        } else {
            res.sendResponse = res.send;
            res.send = (body) => {
                client.set(key, body, (err, reply) => {
                    if (reply === 'OK')
                        console.log('nocached');
                    // res.set('cached', false);
                    res.sendResponse(body)
                })
            };
            next();
        }
    })
};

module.exports = midWare;
