const express = require('express');
const router = express.Router();
const model = require('../models/index');
var midWare = require('../redis');
// GET bank listing.
router.get('/',midWare, async function (req, res, next) {
    try {
        let page = 0;
        let limit = 20;
        let criteria = Object.assign(
            {
                attributes: [
                    ['lvl2jabatan_id', 'golongan_id'],
                    ['urutan_alps', 'kode'],
                    'nama','bu_id'
                ],
                where: {},
            }
        );
        let condition = null;
        if ('bu_id' in req.query) {
            condition = Object.assign({
                where: {bu_id: req.query.bu_id}
            });
        }
        criteria = Object.assign(criteria, condition);
        if ('start' in req.query && 'limit' in req.query) {

            page = parseInt(req.query.start);
            limit = parseInt(req.query.limit);
        }
        criteria = Object.assign(criteria, model.paginate({page: page, pageSize: limit}));
        const level2_jabatans = await model.level2_jabatan.findAndCountAll(
            criteria
        );
        if (level2_jabatans.count !== 0) {
            res.json({
                'status': 'OK',
                'total': level2_jabatans.count,
                'messages': '',
                'results': level2_jabatans.rows
            })
        } else {
            res.json({
                'status': 'ERROR',
                'total': 0,
                'messages': 'EMPTY',
                'results': {}
            })
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err.messages,
            'results': {}
        })
    }
});
router.post('/', async function (req, res, next) {
    try {
        model.sequelize
            .query('SELECT UUID() as uuid;', {plain: true, raw: true, type: model.sequelize.QueryTypes.SELECT})
            .then(data => {
                req.query.lvl2jabatan_id = data.uuid;
                const models = model.level2_jabatan.create({
                    lvl2jabatan_id : req.query.lvl2jabatan_id,
                    urutan_alps:req.query.kode,
                    nama:req.query.nama,
                    bu_id:req.query.bu_id,
                });
                if (models) {
                    res.status(201).json({
                        'success': true,
                        'msg': 'Golongan berhasil ditambahkan',
                        'data': models,
                    })
                }
            });

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});
// UPDATE bank
router.patch('/:id', async function (req, res, next) {
    try {
        const model_id = req.params.id;
        const models = model.level2_jabatan.update({
            urutan_alps:req.query.kode,
            nama:req.query.nama,
            bu_id:req.query.bu_id,
        }, {
            where: {
                lvl2jabatan_id: model_id
            }
        });
        if (models) {
            res.status(201).json({
                'success': true,
                'msg': 'Golongan berhasil diupdate',
                'data': models,
            })
        }

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});



module.exports = router;


