const express = require('express');
const router = express.Router();
const model = require('../models/index');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
var midWare = require('../redis');
// GET bank listing.
router.get('/', midWare, async function (req, res, next) {
    try {
        let page = 0;
        let limit = 20;
        let criteria = Object.assign({
            attributes: [
                [
                    'pegawai_id', 'pegawai_id'
                ],
                [
                    'nik', 'nik'
                ],
                [
                    'lvl2jabatan_id', 'golongan_id'
                ],
                [
                    'lvljabatan_id', 'leveling_id'
                ],
                [
                    'cabang_id', 'cabang_id'
                ],
                [
                    'status_pegawai_id', 'status_id'
                ],
                [
                    'nama_lengkap', 'nama_lengkap'
                ], // tbl person
                [
                    'email', 'email'
                ],
                [
                    'tgl_masuk', 'tgl_masuk'
                ],
                [
                    'status_pegawai_start', 'status_pegawai_start'
                ],
                [
                    'npwp', 'npwp'
                ], // tbl person
                [
                    'ptkp_id', 'status_pegawai_id'
                ], //person
                [
                    'jabatan_id', 'jabatan_id'
                ],
                [
                    'birth_date', 'birth_date'
                ], //person
                [
                    'urutan_romawi', 'kode_level'
                ],
                [
                    'nama_level', 'nama_level'
                ],
                [
                    'urutan_alps', 'kode_golongan'
                ],
                [
                    'nama_jabatan', 'nama_jabatan'
                ],
                [
                    'bu_id', 'bu_id'
                ],
                [
                    'divisi_id', 'divisi_id'
                ],
                'kode_cabang',
                'nama_cabang',
                'area_id'
            ],
            where: {}
        });
        // console.log(criteria);
        let condition = null;
        let where = null;
        if ('divisi_id' in req.query) {
            criteria.where.divisi_id = req.query.divisi_id;
        }
        if ('bu_id' in req.query) {
            criteria.where.bu_id = req.query.bu_id;
        }
        if ('pegawai_id' in req.query) {
            criteria.where.pegawai_id = req.query.pegawai_id;
        }
        if ('cabang' in req.query) {
            where = Object.assign(where, {cabang_id: req.query.cabang})
        }
        if ('leveling' in req.query) {
            where = Object.assign(where, {leveling_id: req.query.leveling})
        }
        // console.log(criteria);
        if ('nik' in req.query) {
            criteria.where.nik = {
                [Op.like]: '%' + req.query.nik + '%'
            };
        }
        if ('nama_lengkap' in req.query) {
            criteria.where.nama_lengkap = {
                [Op.like]: '%' + req.query.nama_lengkap + '%'
            };
        }
        if ('kode_cabang' in req.query) {
            criteria.where.kode_cabang = {
                [Op.like]: '%' + req.query.kode_cabang + '%'
            };
        }
        if ('start' in req.query && 'limit' in req.query) {

            page = parseInt(req.query.start);
            limit = parseInt(req.query.limit);
        }
        // if (!'mode' in req.query) {     criteria = Object.assign(criteria,
        // model.paginate({page: page, pageSize: limit})); }
        const pegawais = await model
            .v_pegawai
            .findAndCountAll(criteria);
        if (pegawais.count !== 0) {
            res.json({'status': 'OK', 'total': pegawais.count, 'messages': '', 'results': pegawais.rows})
        } else {
            res.json({'status': 'ERROR', 'total': 0, 'messages': 'EMPTY', 'results': {}})
        }
    } catch (err) {
        res.json({'status': 'ERROR', 'messages': err.messages, 'results': {}})
    }
});
router.patch('/:id', function (req, res, next) {
    try {
        const model_id = req.params.id;

        return model
            .sequelize
            .transaction(t => {
                return model
                    .pegawai
                    .update({
                        nik: req.query.nik,
                        lvl2jabatan_id: req.query.golongan_id,
                        lvljabatan_id: req.query.leveling_id,
                        cabang_id: req.query.cabang_id,
                        status_pegawai_id: req.query.status_id,
                        email: req.query.email,
                        tgl_masuk: req.query.tgl_masuk,
                        jabatan_id: req.query.jabatan_id
                    }, {
                        where: {
                            pegawai_id: model_id
                        },
                        transaction: t
                    })
                    .then(peg => {
                        return model
                            .status
                            .findOne({
                                where: {
                                    nama_status: 'KARYAWAN'
                                }
                            })
                            .then(stat => {
                                if (stat) {
                                    return model
                                        .family_pegawai
                                        .findOne({
                                            where: {
                                                pegawai_id: model_id,
                                                status_id: stat.status_id
                                            }
                                        })
                                        .then(fampeg => {
                                            if (fampeg) {
                                                return model
                                                    .person
                                                    .update({
                                                        nama_lengkap: req.query.nama_lengkap,
                                                        npwp: req.query.npwp,
                                                        email: req.query.email,
                                                        ptkp_id: req.query.status_pegawai_id,
                                                        birth_date: req.query.birth_date
                                                    }, {
                                                        where: {
                                                            person_id: fampeg.person_id
                                                        },
                                                        transaction: t
                                                    })
                                                    .then(person_ => {
                                                        return model
                                                            .hr_bank
                                                            .destroy({
                                                                where: {
                                                                    person_id: fampeg.person_id
                                                                },
                                                                transaction: t
                                                            })
                                                            .then(() => {
                                                                const promises = [];
                                                                let banks = JSON.parse(req.query.detil);
                                                                let i = 0,
                                                                    len = banks.length;
                                                                for (; i < len; i++) {
                                                                    const uuidPromise = model
                                                                        .hr_bank
                                                                        .create({
                                                                            'hr_bank_id': model
                                                                                .sequelize
                                                                                .fn('UUID'),
                                                                            'bank_id': banks[i].bank_id,
                                                                            'person_id': fampeg.person_id,
                                                                            'tipe_bank': banks[i].tipe_bank,
                                                                            'no_rekening': banks[i].no_rekening,
                                                                            'fotocopy': banks[i].fotocopy
                                                                        }, {transaction: t});
                                                                    promises.push(uuidPromise);
                                                                }
                                                                return Promise.all(promises);
                                                            })
                                                    })
                                            }
                                        });

                                }
                            });

                    });
            })
            .then(result => {
                res
                    .status(201)
                    .json({
                        'success': true, 'msg': 'Pegawai berhasil diupdate',
                        // 'data': result,
                    })

            })
            .catch(err => {
                throw err;
            });

    } catch (err) {
        res
            .status(400)
            .json({'success': 'ERROR', 'messages': err.message, 'data': {}})
    }
});

router.get('/resign', midWare, async function (req, res, next) {
    try {
        let page = 0;
        let limit = 20;
        let criteria = Object.assign({
            attributes: [
                [
                    'pegawai_id', 'pegawai_id'
                ],
                [
                    'nik', 'nik'
                ],
                [
                    'lvl2jabatan_id', 'golongan_id'
                ],
                [
                    'lvljabatan_id', 'leveling_id'
                ],
                [
                    'cabang_id', 'cabang_id'
                ],
                [
                    'status_pegawai_id', 'status_id'
                ],
                [
                    'nama_lengkap', 'nama_lengkap'
                ], // tbl person
                [
                    'email', 'email'
                ],
                [
                    'tgl_masuk', 'tgl_masuk'
                ],
                [
                    'status_pegawai_start', 'status_pegawai_start'
                ],
                [
                    'npwp', 'npwp'
                ], // tbl person
                [
                    'ptkp_id', 'status_pegawai_id'
                ], //person
                [
                    'jabatan_id', 'jabatan_id'
                ],
                [
                    'birth_date', 'birth_date'
                ], //person
                [
                    'urutan_romawi', 'kode_level'
                ],
                [
                    'nama_level', 'nama_level'
                ],
                [
                    'urutan_alps', 'kode_golongan'
                ],
                [
                    'nama_jabatan', 'nama_jabatan'
                ],
                [
                    'bu_id', 'bu_id'
                ],
                'kode_cabang',
                'nama_cabang',
                'area_id'
            ],
            where: {}
        });
        // console.log(criteria);
        let condition = null;
        let where = null;
        criteria.where.status_pegawai_id = '6e772fd4-773c-11e7-9b95-68f7286688e6';
        if ('bu_id' in req.query) {
            criteria.where.bu_id = req.query.bu_id;
        }
        // if ('pegawai_id' in req.query) {     criteria.where.pegawai_id =
        // req.query.pegawai_id; } if ('cabang' in req.query) {     where =
        // Object.assign(where, {         cabang_id: req.query.cabang     }) } if
        // ('leveling' in req.query) {     where = Object.assign(where, {
        // leveling_id: req.query.leveling     }) } if ('nik' in req.query) {
        // criteria.where.nik = {         [Op.like]: '%' + req.query.nik + '%'     }; }
        // if ('nama_lengkap' in req.query) {     criteria.where.nama_lengkap = {
        //  [Op.like]: '%' + req.query.nama_lengkap + '%'     }; } if ('kode_cabang' in
        // req.query) {     criteria.where.kode_cabang = {         [Op.like]: '%' +
        // req.query.kode_cabang + '%'     }; }
        if ('start' in req.query && 'limit' in req.query) {

            page = parseInt(req.query.start);
            limit = parseInt(req.query.limit);
        }
        if (!'mode' in req.query) {
            criteria = Object.assign(criteria, model.paginate({page: page, pageSize: limit}));
        }
        const pegawais = await model
            .v_pegawai
            .findAndCountAll(criteria);
        if (pegawais.count !== 0) {
            res.json({'status': 'OK', 'total': pegawais.count, 'messages': '', 'results': pegawais.rows})
        } else {
            res.json({'status': 'ERROR', 'total': 0, 'messages': 'EMPTY', 'results': {}})
        }
    } catch (err) {
        res.json({'status': 'ERROR', 'messages': err.messages, 'results': {}})
    }
});

module.exports = router;
