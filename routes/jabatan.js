const express = require('express');
const router = express.Router();
const model = require('../models/index');
var midWare = require('../redis');
// GET bank listing.
router.get('/', midWare, async function (req, res, next) {
    try {
        let page = 0;
        let limit = 20;
        let criteria = Object.assign(
            {
                where: {},
            }
        );
        let condition = null;
        if ('bu_id' in req.query) {
            condition = Object.assign({
                where: {bu_id: req.query.bu_id}
            });
        }
        criteria = Object.assign(criteria, condition);
        // console.log(condition);
        if ('start' in req.query && 'limit' in req.query) {

            page = parseInt(req.query.start);
            limit = parseInt(req.query.limit);
        }
        if (!'mode' in req.query && req.query.mode === 'grid') {
            criteria = Object.assign(criteria, model.paginate({page: page, pageSize: limit}));
        }
        const jabatans = await model.jabatan.findAndCountAll(
            criteria
        );
        if (jabatans.count !== 0) {
            res.json({
                'status': 'OK',
                'total': jabatans.count,
                'messages': '',
                'results': jabatans.rows
            })
        } else {
            res.json({
                'status': 'ERROR',
                'total': 0,
                'messages': 'EMPTY',
                'results': {}
            })
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err.messages,
            'results': {}
        })
    }
});

router.post('/', async function (req, res, next) {
    try {
        model.sequelize
            .query('SELECT UUID() as uuid;', {plain: true, raw: true, type: model.sequelize.QueryTypes.SELECT})
            .then(data => {
                req.query.jabatan_id = data.uuid;
                const models = model.jabatan.create(req.query);
                if (models) {
                    res.status(201).json({
                        'success': true,
                        'msg': 'Jabatan berhasil ditambahkan',
                        'data': models,
                    })
                }
            });

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});
// UPDATE bank
router.patch('/:id', async function (req, res, next) {
    try {
        const model_id = req.params.id;
        const models = model.jabatan.update(req.query, {
            where: {
                jabatan_id: model_id
            }
        });
        if (models) {
            res.status(201).json({
                'success': true,
                'msg': 'Jabatan berhasil diupdate',
                'data': models,
            })
        }

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});


module.exports = router;


