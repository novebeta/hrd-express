const express = require('express');
const router = express.Router();
const model = require('../models/index');
var midWare = require('../redis');
// GET bank listing.
router.get('/', midWare, async function (req, res, next) {
    try {
        let page = 0;
        let limit = 20;
        let criteria = Object.assign(
            {
                where: {}, // conditions
            }
        );
        if('bank_id' in req.query){
            criteria.where.bank_id = req.query.bank_id;
        }
        if ('start' in req.query && 'limit' in req.query) {

            page = parseInt(req.query.start);
            limit = parseInt(req.query.limit);
        }
        criteria = Object.assign(criteria, model.paginate({page: page, pageSize: limit}));
        const banks = await model.bank.findAndCountAll(
            criteria
        );
        if (banks.count !== 0) {
            res.json({
                'status': 'OK',
                'total': banks.count,
                'messages': '',
                'results': banks.rows
            })
        } else {
            res.json({
                'status': 'ERROR',
                'total': 0,
                'messages': 'EMPTY',
                'results': {}
            })
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err.messages,
            'results': {}
        })
    }
});
// POST bank
router.post('/', async function (req, res, next) {
    try {
        model.sequelize
            .query('SELECT UUID() as uuid;', {plain: true, raw: true, type: model.sequelize.QueryTypes.SELECT})
            .then(data => {
                const banks = model.bank.create({
                    bank_id: data.uuid,
                    nama_bank: req.query.nama_bank
                });
                if (banks) {
                    res.status(201).json({
                        'success': true,
                        'msg': 'Bank berhasil ditambahkan',
                        'data': banks,
                    })
                }
            });

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});
// UPDATE bank
router.patch('/:id', async function (req, res, next) {
    try {
        // console.log(req);
        const bankId = req.params.id;
        const banks = model.bank.update({
            nama_bank: req.query.nama_bank
        }, {
            where: {
                bank_id: bankId
            }
        });
        if (banks) {
            res.status(201).json({
                'success': true,
                'msg': 'Bank berhasil diupdate',
                'data': banks,
            })
        }

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});

module.exports = router;


