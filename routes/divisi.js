const express = require('express');
const router = express.Router();
const model = require('../models/index');
var midWare = require('../redis');
// GET bank listing.
router.get('/', midWare, async function (req, res, next) {
    try {
        let page = 0;
        let limit = 20;
        let criteria = Object.assign(
            {
                where: {},
            }
        );
        let condition = null;
        if ('bu_id' in req.query) {
            condition = Object.assign({
                where: {bu_id: req.query.bu_id}
            });
        }
        if ('divisi_id' in req.query) {
            condition = Object.assign({
                where: {divisi_id: req.query.divisi_id}
            });
        }
        criteria = Object.assign(criteria, condition);
        // console.log(condition);
        if ('start' in req.query && 'limit' in req.query) {

            page = parseInt(req.query.start);
            limit = parseInt(req.query.limit);
        }
        if (!'mode' in req.query && req.query.mode === 'grid') {
            criteria = Object.assign(criteria, model.paginate({page: page, pageSize: limit}));
        }
        const divisis = await model.divisi.findAndCountAll(
            criteria
        );
        if (divisis.count !== 0) {
            res.json({
                'status': 'OK',
                'total': divisis.count,
                'messages': '',
                'results': divisis.rows
            })
        } else {
            res.json({
                'status': 'ERROR',
                'total': 0,
                'messages': 'EMPTY',
                'results': {}
            })
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err.messages,
            'results': {}
        })
    }
});

router.post('/', async function (req, res, next) {
    try {
        model.sequelize
            .query('SELECT UUID() as uuid;', {plain: true, raw: true, type: model.sequelize.QueryTypes.SELECT})
            .then(data => {
                req.query.divisi_id = data.uuid;
                const models = model.divisi.create(req.query);
                if (models) {
                    res.status(201).json({
                        'success': true,
                        'msg': 'divisi berhasil ditambahkan',
                        'data': models,
                    })
                }
            });

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});
// UPDATE bank
router.patch('/:id', async function (req, res, next) {
    try {
        const model_id = req.params.id;
        const models = model.divisi.update(req.query, {
            where: {
                divisi_id: model_id
            }
        });
        if (models) {
            res.status(201).json({
                'success': true,
                'msg': 'divisi berhasil diupdate',
                'data': models,
            })
        }

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});


module.exports = router;


