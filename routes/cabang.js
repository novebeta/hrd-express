const express = require('express');
const router = express.Router();
const model = require('../models/index');
var midWare = require('../redis');
// GET bank listing.
router.get('/',midWare, async function (req, res, next) {
    try {
        let page = 0;
        let limit = 20;
        let criteria = Object.assign(
            {
                where: {}, // conditions
            }
        );
        let condition = null;
        let where = null;
        if ('bu_id' in req.query) {
            where = Object.assign({
                bu_id: req.query.bu_id
            })
        }
        if ('cabang_id' in req.query) {
            where = Object.assign(where, {
                cabang_id: req.query.cabang_id
            })
        }
        

        if (where != null) {
            condition = Object.assign({
                where: where
            });
        }
        criteria = Object.assign(criteria, condition);
        if (!'start' in req.query && !'limit' in req.query) {

            page = parseInt(req.query.start);
            limit = parseInt(req.query.limit);
        }
        // if(!'mode' in req.query) {
        //     criteria = Object.assign(criteria, model.paginate({page: page, pageSize: limit}));
        // }
        

        if ('pt_id' in req.query) {
            criteria.include = [{
                model: model.bu,
                where: {pt_id:req.query.pt_id}
            }];
        }

        const cabangs = await model.cabang.findAndCountAll(
            criteria
        );
        if (cabangs.count !== 0) {
            res.json({
                'status': 'OK',
                'total': cabangs.count,
                'messages': '',
                'results': cabangs.rows
            })
        } else {
            res.json({
                'status': 'ERROR',
                'total': 0,
                'messages': 'EMPTY',
                'results': {}
            })
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err.messages,
            'results': {}
        })
    }
});


router.post('/', async function (req, res, next) {
    try {
        if(!'cabang_id' in req.query){
            req.query.cabang_id = model.sequelize.fn('UUID');
        }
        const models = model.cabang.create(req.query);
        if (models) {
            res.status(201).json({
                'success': true,
                'msg': 'Cabang berhasil ditambahkan',
                'data': models,
            })
        }

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});
// UPDATE bank
router.patch('/:id', async function (req, res, next) {
    try {
        const model_id = req.params.id;
        const models = model.cabang.update(req.query, {
            where: {
                cabang_id: model_id
            }
        });
        if (models) {
            res.status(201).json({
                'success': true,
                'msg': 'Cabang berhasil diupdate',
                'data': models,
            })
        }

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});


module.exports = router;


