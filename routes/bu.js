const express = require('express');
const router = express.Router();
const model = require('../models/index');
var midWare = require('../redis');
// GET bank listing.
router.get('/',midWare, async function (req, res, next) {
    try {
        let page = 0;
        let limit = 20;
        let criteria = Object.assign(
            {
                where: {},
            }
        );
        // let condition = null;
        if ('bu_id' in req.query) {
            criteria.where.bu_id = req.query.bu_id;
        }
        if ('select' in req.query) {
            criteria.attributes = Object.keys(req.query.select).map(function(key) {
                return [key, req.query.select[key]];
            });
        }
        // criteria = Object.assign(criteria,condition);
        console.log('Request : ' + req.query);
        if ('start' in req.query &&  'limit' in req.query) {

            page = parseInt(req.query.start);
            limit = parseInt(req.query.limit);
        }
        if (req.query.mode === 'grid') {
            criteria = Object.assign(criteria, model.paginate({page: page, pageSize: limit}));
        }
        const bus = await model.bu.findAndCountAll(
            criteria
        );
        if (bus.count !== 0) {
            res.json({
                'status': 'OK',
                'total': bus.count,
                'messages': '',
                'results': bus.rows
            })
        } else {
            res.json({
                'status': 'ERROR',
                'total': 0,
                'messages': 'EMPTY',
                'results': {}
            })
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err.messages,
            'results': {}
        })
    }
});
// router.post('/', async function (req, res, next) {
//     try {
//         model.sequelize
//             .query('SELECT UUID() as uuid;', {plain: true, raw: true, type: model.sequelize.QueryTypes.SELECT})
//             .then(data => {
//                 req.query.bu_id = data.uuid;
//                 const bus = model.bu.create(req.query);
//                 if (bus) {
//                     res.status(201).json({
//                         'success': true,
//                         'msg': 'Bisnis Unit berhasil ditambahkan',
//                         'data': bus,
//                     })
//                 }
//             });
//
//     } catch (err) {
//         res.status(400).json({
//             'success': 'ERROR',
//             'messages': err.message,
//             'data': {},
//         })
//     }
// });
// UPDATE bank
router.patch('/:id', async function (req, res, next) {
    try {
        console.log(req.query);
        const buId = req.params.id;
        const bus = model.bu.update(req.query, {
            where: {
                bu_id: buId
            }
        });
        if (bus) {
            res.status(201).json({
                'success': true,
                'msg': 'Bisnis Unit berhasil diupdate',
                'data': bus,
            })
        }

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});



module.exports = router;


