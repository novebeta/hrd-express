const express = require('express');
const router = express.Router();
const model = require('../models/index');
var midWare = require('../redis');
// GET bank listing.
router.get('/', async function (req, res, next) {
    try {
        // let page = 0;
        // let limit = 20;
        let criteria = Object.assign({
                attributes: [
                    ['hr_bank_id', 'pegawai_bank_id'],
                    ['bank_id', 'bank_id'],
                    ['tipe_bank', 'tipe_bank'],
                    ['no_rekening', 'no_rekening'],
                    ['fotocopy', 'fotocopy'],
                    ['visible', 'visible'],
                    ['person_id', 'pegawai_id']
                ],
                where: {
                    visible: 1
                },
            }
        );
        let condition = null;
        if('tipe_bank' in req.query){
            criteria.where.tipe_bank = req.query.tipe_bank;
        }
        if ('pegawai_id' in req.query) {
            model.pegawai.getPersonId(req.query.pegawai_id, async function (person_id) {
                console.log(person_id);
                criteria.where.person_id = person_id;


                if('nama_bank' in req.query){
                    model.hr_bank.getBankId(req.query.nama_bank, async function (bank_id) {
                        criteria.where.bank_id = bank_id;
                        const hr_banks = await model.hr_bank.findAndCountAll(
                            criteria
                        );
                        if (hr_banks.count !== 0) {
                            res.json({
                                'status': 'OK',
                                'total': hr_banks.count,
                                'messages': '',
                                'results': hr_banks.rows
                            })
                        } else {
                            res.json({
                                'status': 'ERROR',
                                'total': 0,
                                'messages': 'EMPTY',
                                'results': {}
                            })
                        }
                    });
                }
                const hr_banks = await model.hr_bank.findAndCountAll(
                    criteria
                );
                if (hr_banks.count !== 0) {
                    res.json({
                        'status': 'OK',
                        'total': hr_banks.count,
                        'messages': '',
                        'results': hr_banks.rows
                    })
                } else {
                    res.json({
                        'status': 'ERROR',
                        'total': 0,
                        'messages': 'EMPTY',
                        'results': {}
                    })
                }
            });
            //criteria.where.person_id =
        } else {
            const hr_banks = await model.hr_bank.findAndCountAll(
                criteria
            );
            if (hr_banks.count !== 0) {
                res.json({
                    'status': 'OK',
                    'total': hr_banks.count,
                    'messages': '',
                    'results': hr_banks.rows
                })
            } else {
                res.json({
                    'status': 'ERROR',
                    'total': 0,
                    'messages': 'EMPTY',
                    'results': {}
                })
            }
        }
        // console.log('skip person_id');
        // criteria = Object.assign(criteria, condition);
        // if ('start' in req.query &&  'limit' in req.query) {
        //
        //     page = parseInt(req.query.start);
        //     limit = parseInt(req.query.limit);
        // }
        // criteria = Object.assign(criteria, model.paginate({page: page, pageSize: limit}));

    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err.messages,
            'results': {}
        })
    }
});


router.post('/', async function (req, res, next) {
    try {
        model.sequelize
            .query('SELECT UUID() as uuid;', {plain: true, raw: true, type: model.sequelize.QueryTypes.SELECT})
            .then(data => {
                req.query.hr_bank_id = data.uuid;
                const models = model.hr_bank.create(req.query);
                if (models) {
                    res.status(201).json({
                        'success': true,
                        'msg': 'Rekening berhasil ditambahkan',
                        'data': models,
                    })
                }
            });

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});
// UPDATE bank
router.patch('/:id', async function (req, res, next) {
    try {
        console.log(req.query);
        const model_id = req.params.id;
        const models = model.hr_bank.update(req.query, {
            where: {
                hr_bank_id: model_id
            }
        });
        if (models) {
            res.status(201).json({
                'success': true,
                'msg': 'Rekening berhasil diupdate',
                'data': models,
            })
        }

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});


module.exports = router;


