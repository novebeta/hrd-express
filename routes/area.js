const express = require('express');
const router = express.Router();
const model = require('../models/index');
var midWare = require('../redis');
// GET bank listing.
router.get('/',midWare, async function (req, res, next) {
    try {
        let page = 0;
        let limit = 20;
        let criteria = Object.assign(
            {
                where: {},
            }
        );
        let condition = null;
        if ('bu_id' in req.query) {
            condition = Object.assign({
                where: {bu_id: req.query.bu_id}
            });
        }
        criteria = Object.assign(criteria,condition);
        // console.log(condition);
        if ('start' in req.query &&  'limit' in req.query) {

            page = parseInt(req.query.start);
            limit = parseInt(req.query.limit);
        }
        criteria = Object.assign(criteria, model.paginate({page: page, pageSize: limit}));
        const areas = await model.area.findAndCountAll(
            criteria
        );
        if (areas.count !== 0) {
            res.json({
                'status': 'OK',
                'total': areas.count,
                'messages': '',
                'results': areas.rows
            })
        } else {
            res.json({
                'status': 'ERROR',
                'total': 0,
                'messages': 'EMPTY',
                'results': {}
            })
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err.messages,
            'results': {}
        })
    }
});

router.post('/', async function (req, res, next) {
    try {
        model.sequelize
            .query('SELECT UUID() as uuid;', {plain: true, raw: true, type: model.sequelize.QueryTypes.SELECT})
            .then(data => {
                req.query.area_id = data.uuid;
                const models = model.area.create(req.query);
                if (models) {
                    res.status(201).json({
                        'success': true,
                        'msg': 'Area berhasil ditambahkan',
                        'data': models,
                    })
                }
            });

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});
// UPDATE bank
router.patch('/:id', async function (req, res, next) {
    try {
        console.log(req.query);
        const model_id = req.params.id;
        const models = model.area.update(req.query, {
            where: {
                area_id: model_id
            }
        });
        if (models) {
            res.status(201).json({
                'success': true,
                'msg': 'Area berhasil diupdate',
                'data': models,
            })
        }

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});


module.exports = router;


