const express = require('express');
const router = express.Router();
const model = require('../models/index');
var midWare = require('../redis');
// GET bank listing.
router.get('/', midWare, async function (req, res, next) {
    try {
        let page = 0;
        let limit = 20;
        let criteria = Object.assign({
                attributes: [
                    ['ptkp_id', 'status_pegawai_id'],
                    ['ptkp_kode', 'kode'],
                    ['ptkp_nama', 'nama_status']
                ],
                where: {},
            }
        );
        let condition = null;

        // criteria = Object.assign(criteria,condition);
        // console.log(condition);
        if ('start' in req.query && 'limit' in req.query) {

            page = parseInt(req.query.start);
            limit = parseInt(req.query.limit);
        }
        criteria = Object.assign(criteria, model.paginate({page: page, pageSize: limit}));
        const ptkps = await model.ptkp.findAndCountAll(
            criteria
        );
        if (ptkps.count !== 0) {
            res.json({
                'status': 'OK',
                'total': ptkps.count,
                'messages': '',
                'results': ptkps.rows
            })
        } else {
            res.json({
                'status': 'ERROR',
                'total': 0,
                'messages': 'EMPTY',
                'results': {}
            })
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err.messages,
            'results': {}
        })
    }
});


router.post('/', async function (req, res, next) {
    try {
        model.sequelize
            .query('SELECT UUID() as uuid;', {plain: true, raw: true, type: model.sequelize.QueryTypes.SELECT})
            .then(data => {
                req.query.ptkp_id = data.uuid;
                const models = model.ptkp.create({
                    ptkp_id: req.query.ptkp_id,
                    ptkp_kode: req.query.kode,
                    ptkp_nama: req.query.nama_status
                });
                if (models) {
                    res.status(201).json({
                        'success': true,
                        'msg': 'Status Pegawai berhasil ditambahkan',
                        'data': models,
                    })
                }
            });

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});
// UPDATE bank
router.patch('/:id', async function (req, res, next) {
    try {
        const model_id = req.params.id;
        const models = model.ptkp.update({
            ptkp_kode: req.query.kode,
            ptkp_nama: req.query.nama_status
        }, {
            where: {
                ptkp_id: model_id
            }
        });
        if (models) {
            res.status(201).json({
                'success': true,
                'msg': 'Status Pegawai berhasil diupdate',
                'data': models,
            })
        }

    } catch (err) {
        res.status(400).json({
            'success': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
});


module.exports = router;


