/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('test_jabatan', {
    jabatan: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'test_jabatan'
  });
};
