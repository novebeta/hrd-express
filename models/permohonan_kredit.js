/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('permohonan_kredit', {
    permohonan_kredit_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    jenis_kredit: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    sk_tujuan_kredit: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    jumlah: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0'
    },
    sk_jangka_waktu: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0'
    },
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'pegawai',
        key: 'pegawai_id'
      }
    },
    almt_tinggal: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    almt_ktp: {
      type: DataTypes.STRING(350),
      allowNull: true
    },
    npwp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    nama_gadis_ibu: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    nama_pasangan: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    pendidikan_terakhir: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    status_rumah: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    tanggungan_keluarga: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    kel_nama: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    kel_alamat: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    kel_phone: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    kel_hub: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    nama_lengkap: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    nama_cabang: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    nama_jabatan: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    sk_nominal: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00'
    },
    sk_fasilitas: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    sk_suku_bunga: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    sk_provisi: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00'
    },
    sk_administrasi: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    sk_angsuran_per_bulan: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0.00'
    },
    sk_syarat_lain_lain: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    mk_user_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    mk_tdate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    mk_doc_ref: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    sk_doc_ref: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    sk_tdate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    sk_user_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    jangka_waktu: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0'
    },
    mk_tgl: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    sk_tgl: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    reject: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      defaultValue: '0'
    },
    no_pengajuan: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    no_perjanjian: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    no_notifikasi: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    no_rekening: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    tgl_mulai: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    tgl_berakhir: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    tgl_angsur_pertama: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    rt_ktp: {
      type: DataTypes.STRING(3),
      allowNull: true
    },
    rw_ktp: {
      type: DataTypes.STRING(3),
      allowNull: true
    },
    kel_desa_ktp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    kecamatan_ktp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    kabupaten_ktp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    propinsi_ktp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    kel_phone_kantor: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    cabang_kantor: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    no_tlp_kantor: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    nama_kacap_kantor: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    tgl_lahir: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    tempat_lahir: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    sk_asuransi: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    mk_no_rekening: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    mk_nama_rekening: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    mk_bank_rekening: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    pekerjaan: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    ktp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    hp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    telp_rumah: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    birth_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    no_telp_cabang: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    alamat_cabang: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    status_kawin: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      defaultValue: '0'
    },
    nik: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    sex: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    jenis_tabungan_bpr: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    nama_ao: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    ket: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    nama_p: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    birth_date_p: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    pekerjaan_p: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    almt_tgl_p: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ktp_p: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    note_kacab: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    note_hrd: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    gajipokok: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    statusint: {
      type: DataTypes.CHAR(2),
      allowNull: true,
      defaultValue: '0'
    },
    reject_ket: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    total_terima: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    note_bank: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'permohonan_kredit'
  });
};
