/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('family_pegawai', {
    family_pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    status_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'status',
        key: 'status_id'
      }
    },
    person_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'person',
        key: 'person_id'
      }
    },
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    rec_status: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '0'
    },
    status_edit: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    tdate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    binding_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'pegawai',
        key: 'binding_id'
      }
    },
    priorityid: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'family_pegawai'
  });
};
