/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('fasilitas_pegawai', {
    fasilitas_pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    fasilitas_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    keterangan: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'fasilitas_pegawai'
  });
};
