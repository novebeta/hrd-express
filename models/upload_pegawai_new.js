/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('upload_pegawai_new', {
    kode_cabang: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nik: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nama_lengkap: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    panggilan: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nik_lokal: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    tgl_masuk: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    tgl_masuk_real: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    divisi: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    jabatan: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    pertgl: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    status_pegawai: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    hubungan_kerja: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    agama: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    status_ptkp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    status_kekaryaan: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    level: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    golongan: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    tgl_lahir: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    instagram: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    facebook: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    linkedin: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    line: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    whatsapp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ktp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    hp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    alamat_tinggal: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    alamat_ktp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    sex: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    kota_lahir: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    norek_bpr: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    norek_mandiri: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    pendidikan: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    jurusan: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    univ: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    no_npwp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nama_npwp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    alamat_npwp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    no_kk: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    kepala_keluarga_kk: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    alamt_kk: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    no_bpjs_kesehatan: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    faskes: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    kelas_rawat: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    no_bpjs_ketenagakerjaan: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    no_bpjs_jamsos: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    pengalaman_internal: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    pengalaman_eksternal: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nama_pasangan: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ktp_pasangan: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    tgl_pasangan: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nama_bapak: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    no_ktp_bapak: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    tgl_bpk: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nama_ibu: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    no_ktp_ibu: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    tgl_ibu: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nama_anak1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ktp_anak1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    tgl_anak1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nama_anak2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ktp_anak2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    tgl_anak2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nama_anak3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ktp_anak3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    tgl_anak3: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nama_anak4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ktp_anak4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    tgl_anak4: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nama_anak5: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ktp_anak5: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    tgl_anak5: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'upload_pegawai_new'
  });
};
