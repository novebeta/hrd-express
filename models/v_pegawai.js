/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('v_pegawai', {
    nik_display: {
      type: DataTypes.STRING(32),
      allowNull: true
    },
    nama_lengkap: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    person_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    person_up_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    family_pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    binding_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    pegawai_up_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    rec_status_1: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '0'
    },
    rec_status_2: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '0'
    },
    rec_status_3: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '0'
    },
    birth_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    birth_date_display: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    nick_name: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ktp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    ktp_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    hp: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    almt_tinggal: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    almt_ktp: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    nama_gadis_ibu: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    nama_pasangan: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    pendidikan_terakhir: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    status_rumah: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    tanggungan_keluarga: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    kel_nama: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    kel_alamat: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    kel_phone: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    kel_hub: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    expired_ktp: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    sex: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    pekerjaan: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    telp_rumah: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    kel_phone_kantor: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    tempat_lahir: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    no_rekening: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    bn_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    no_rekening_mandiri: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    bm_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    pt_jenjang: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    pt_jurusan: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    pt_unv: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    npwp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    npwp_nama: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    npwp_almt: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    npwp_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    kk_no: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    kk_nama: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    kk_almt: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    kk_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    bpjskes_no: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    bpjskes_nama_faskes1: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    bpjskes_kelas_rawat: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    bpjskes_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    bpjsket_no: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    bpjsket_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    bpjsjam_no: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    bpjsjam_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    wex_internal: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    wex_eksternal: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    keterangan: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    status_kawin: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    user_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    agama: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ptkp_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    ptkp_kode: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    ptkp_nama: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    status_kekaryaan_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    kode_status_kekaryaan: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nama_status_kekaryaan: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    status_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    nama_status: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    seq: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    nik: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    nik_create: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    nik_lokal: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    tgl_masuk: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    tgl_masuk_real: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    card_number: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    status_kerja: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    active: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: '1'
    },
    cabang_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    cabang_start: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    divisi_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    divisi_jabatan_start: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    divisi_jabatan_estend: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    divisi_nama: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    divisi_kode: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    jabatan_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    kode_jabatan: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    nama_jabatan: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    urutan: {
      type: DataTypes.INTEGER(3),
      allowNull: true,
      defaultValue: '0'
    },
    lvljabatan_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    nama_level: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    urutan_romawi: {
      type: DataTypes.STRING(10),
      allowNull: true,
      defaultValue: ''
    },
    lvl2jabatan_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    urutan_alps: {
      type: DataTypes.STRING(10),
      allowNull: true,
      defaultValue: ''
    },
    status_pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    status_pegawai_start: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    status_pegawai_estend: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    status_pegawai_kode: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    status_pegawai_nama: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    hubungan_kerja_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    hubungan_kerja_start: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    hubungan_kerja_estend: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    hubungan_kerja_nama: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    hubungan_kerja_kode: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    pegawai_sync: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    pegawai_imp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    kode_cabang: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    nama_cabang: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    bu_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    bu_kode: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    bu_name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    pt_kode: {
      type: DataTypes.STRING(2),
      allowNull: false
    },
    pt_name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    bu_private: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    instagram: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    facebook: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    linkedin: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    whatsapp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    line: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    area_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    }
  }, {
    tableName: 'v_pegawai'
  });
};
