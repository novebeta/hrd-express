/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('permohonan_kredit_approval', {
    permohonan_kredit_approval_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    permohonan_kredit_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'permohonan_kredit',
        key: 'permohonan_kredit_id'
      }
    },
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    tgl_approval: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    security_roles_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    tableName: 'permohonan_kredit_approval'
  });
};
