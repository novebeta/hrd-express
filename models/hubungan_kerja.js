/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hubungan_kerja', {
    hubungan_kerja_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    hubungan_kerja_kode: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    hubungan_kerja_nama: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    urutan: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'hubungan_kerja'
  });
};
