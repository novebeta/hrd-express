/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('area', {
    area_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    kode: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    nama: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    bu_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    }
  }, {
    tableName: 'area'
  });
};
