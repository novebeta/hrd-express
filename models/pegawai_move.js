/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pegawai_move', {
    pegawai_move_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    trans_date: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    tdate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'pegawai',
        key: 'pegawai_id'
      }
    },
    cabang_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'cabang',
        key: 'cabang_id'
      }
    },
    jabatan_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'jabatan',
        key: 'jabatan_id'
      }
    },
    nama_pj: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'pegawai_move'
  });
};
