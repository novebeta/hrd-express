/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hr_member_card', {
    hr_member_card_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'pegawai',
        key: 'pegawai_id'
      }
    },
    no_kartu: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    nik: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    nama_lengkap: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    tgl_generate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    tgl_masuk: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    bu_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'bu',
        key: 'bu_id'
      }
    },
    visible: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '1'
    },
    statuscetak: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    since: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    valid: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    active: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    printdate: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'hr_member_card'
  });
};
