/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    const Model = sequelize.define('hr_bank', {
        hr_bank_id: {
            type: DataTypes.STRING(36),
            allowNull: false,
            primaryKey: true
        },
        bank_id: {
            type: DataTypes.STRING(36),
            allowNull: false
        },
        person_id: {
            type: DataTypes.STRING(36),
            allowNull: false
        },
        tipe_bank: {
            type: DataTypes.STRING(10),
            allowNull: false
        },
        no_rekening: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        fotocopy: {
            type: DataTypes.INTEGER(1),
            allowNull: true,
            defaultValue: '0'
        },
        visible: {
            type: DataTypes.INTEGER(1),
            allowNull: true,
            defaultValue: '1'
        },
        user_id: {
            type: DataTypes.STRING(36),
            allowNull: true
        },
        tdate: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'hr_bank'
    });

    Model.getBankId = function (nama_bank, fn) {
        sequelize.models.bank.findOne({where: {nama_bank: nama_bank}})
            .then(stat => {
                fn(stat.bank_id);
            });

    };

    return Model;

};
