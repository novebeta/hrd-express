/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    username: {
      type: DataTypes.STRING(60),
      allowNull: false
    },
    password: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    last_visit_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    active: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: '1'
    },
    cabang_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    security_roles_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'security_roles',
        key: 'security_roles_id'
      }
    },
    bu_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    }
  }, {
    tableName: 'users'
  });
};
