/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('level2_jabatan', {
    lvl2jabatan_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    urutan: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '1'
    },
    urutan_alps: {
      type: DataTypes.STRING(10),
      allowNull: true,
      defaultValue: ''
    },
    nama: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    bu_id: {
      type: DataTypes.STRING(36),
      allowNull: true,
      references: {
        model: 'bu',
        key: 'bu_id'
      }
    }
  }, {
    tableName: 'level2_jabatan'
  });
};
