/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('history_card', {
    history_card_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    birth_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    nama_lengkap: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    nick_name: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ktp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    hp: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    nik: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    tgl_masuk: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    card_number: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    status_kerja: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    bu_name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    bu_nama_alias: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    nama_jabatan: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    kode_cabang: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    nama_cabang: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    tgl_keluar: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    tdate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    user_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    }
  }, {
    tableName: 'history_card'
  });
};
