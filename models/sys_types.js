/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sys_types', {
    sys_types_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    type_id: {
      type: DataTypes.INTEGER(6),
      allowNull: false
    },
    type_no: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    next_reference: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    tableName: 'sys_types'
  });
};
