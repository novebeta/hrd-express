/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('roomcom', {
    roomcom_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    tdateroom: {
      type: DataTypes.DATE,
      allowNull: false
    },
    subject: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    status: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    urutan: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    cabang_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    user_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    }
  }, {
    tableName: 'roomcom'
  });
};
