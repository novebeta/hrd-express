/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hr_medsos', {
    hr_medsos_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    person_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    instagram: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    facebook: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    linkedin: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    line: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    whatsapp: {
      type: DataTypes.STRING(50),
      allowNull: true
    }
  }, {
    tableName: 'hr_medsos'
  });
};
