/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('status', {
    status_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    nama_status: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    seq: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'status'
  });
};
