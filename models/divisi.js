/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('divisi', {
    divisi_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    divisi_kode: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    divisi_nama: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    bu_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    }
  }, {
    tableName: 'divisi'
  });
};
