/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pt', {
    pt_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    pt_nama: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    pt_kode: {
      type: DataTypes.STRING(2),
      allowNull: false
    },
    pt_alias: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    last_kode: {
      type: DataTypes.STRING(10),
      allowNull: false,
      defaultValue: '01'
    },
    bentuk: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    tgl_berdiri: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    jenis_usaha: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    direktur: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    almt: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    profile: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'pt'
  });
};
