/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('angsuran', {
    angsuran_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    tgl_jatuh_tempo: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    permohonan_kredit_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    }
  }, {
    tableName: 'angsuran'
  });
};
