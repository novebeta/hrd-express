/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('status_pegawai', {
    status_pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    status_pegawai_kode: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    status_pegawai_nama: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    urutan: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'status_pegawai'
  });
};
