/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('person_apply', {
    apply_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    apply_no: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    bu_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    nama_lengkap: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    birth_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    nick_name: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ktp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    ktp_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    hp: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    almt_tinggal: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    almt_ktp: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    nama_gadis_ibu: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    nama_pasangan: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    pendidikan_terakhir: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    status_rumah: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    tanggungan_keluarga: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    kel_nama: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    kel_alamat: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    kel_phone: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    kel_hub: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    expired_ktp: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    sex: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    pekerjaan: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    telp_rumah: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    kel_phone_kantor: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    tempat_lahir: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    no_rekening: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    bn_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    no_rekening_mandiri: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    bm_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    pt_jenjang: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    pt_jurusan: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    pt_unv: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    npwp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    npwp_nama: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    npwp_almt: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    npwp_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    kk_no: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    kk_nama: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    kk_almt: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    kk_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    bpjskes_no: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    bpjskes_nama_faskes1: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    bpjskes_kelas_rawat: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    bpjskes_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    bpjsket_no: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    bpjsket_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    bpjsjam_no: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    bpjsjam_fc: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    wex_internal: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    wex_eksternal: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    keterangan: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    status_kawin: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    tdate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    rec_status: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '1'
    },
    user_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    agama: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    parent_update_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    ptkp_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    }
  }, {
    tableName: 'person_apply'
  });
};
