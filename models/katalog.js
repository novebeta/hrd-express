/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('katalog', {
    katalog_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    nama_promosi: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    ketentuan: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    tdate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    user_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    bu_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    }
  }, {
    tableName: 'katalog'
  });
};
