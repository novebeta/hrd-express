/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hr_agama', {
    agama: {
      type: DataTypes.STRING(20),
      allowNull: false,
      primaryKey: true
    }
  }, {
    tableName: 'hr_agama'
  });
};
