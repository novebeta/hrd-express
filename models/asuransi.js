/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('asuransi', {
    asuransi_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    bulan: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    persen: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, {
    tableName: 'asuransi'
  });
};
