/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('request_transfer_client', {
    request_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    from_nama_pemohon: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    almt_ktp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    to_nama_bank: {
      type: DataTypes.STRING(25),
      allowNull: false
    },
    to_no_rekening: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    amount: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    admin_note: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(20),
      allowNull: true,
      defaultValue: 'Menunggu'
    },
    doc_ref: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    to_nama_rekening: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    from_no_rekening: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    no_telp: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    rtc_tdate: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'request_transfer_client'
  });
};
