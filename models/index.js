'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
console.log(env);
const config = require(__dirname + '/../config/config.json')[env];
const db = {};
const paginate = ({page, pageSize}) => {
    const offset = page; // * pageSize;
    const limit = pageSize;

    return {
        offset,
        limit,
    }
};
let sequelize;
if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
    sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs.readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        const model = sequelize['import'](path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

// const generateUUID = () => {
//     sequelize
//         .query('SELECT UUID();', {plain: true, raw: true})
//         .then(uuid => {
//             // console.log(uuid);
//             return uuid;
//         })
// };

// db.generateUUID = generateUUID;
db.sequelize = sequelize;
db.paginate = paginate;

db.bu.hasMany(db.cabang, {foreignKey: 'bu_id'})
db.cabang.belongsTo(db.bu, {foreignKey: 'bu_id'})

module.exports = db;
