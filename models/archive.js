/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('archive', {
    id: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true
    },
    createdAt: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    fromModel: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    originalRecord: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    originalRecordId: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'archive'
  });
};
