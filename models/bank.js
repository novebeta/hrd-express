/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('bank', {
    bank_id: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true
    },
    nama_bank: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'bank'
  });
};
