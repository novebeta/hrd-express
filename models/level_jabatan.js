/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('level_jabatan', {
    lvljabatan_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    nama_level: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    bu_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    urutan: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '1'
    },
    urutan_romawi: {
      type: DataTypes.STRING(10),
      allowNull: true,
      defaultValue: ''
    }
  }, {
    tableName: 'level_jabatan'
  });
};
