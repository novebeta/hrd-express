/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('info', {
    info_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    value: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'info'
  });
};
