/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('fasilitas_kantor', {
    fasilitas_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    fasilitias_jenis: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    fasilitas_desc: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'fasilitas_kantor'
  });
};
