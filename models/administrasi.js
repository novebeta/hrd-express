/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('administrasi', {
    administrasi_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    menu: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    biaya: {
      type: DataTypes.DECIMAL,
      allowNull: false
    }
  }, {
    tableName: 'administrasi'
  });
};
