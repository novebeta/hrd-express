/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('k_ena', {
    k_ena_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    nik: {
      type: DataTypes.STRING(12),
      allowNull: true
    },
    nama_lengkap: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    cabang_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    bu_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER(2),
      allowNull: true
    },
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    kartu_id: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'k_ena'
  });
};
