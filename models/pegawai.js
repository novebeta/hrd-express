/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  const Model = sequelize.define('pegawai', {
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    pegawai_up_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    nik: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    nik_create: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    },
    nik_lokal: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    tgl_masuk: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    tgl_masuk_real: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    card_number: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    status_kerja: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    active: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: '1'
    },
    cabang_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'cabang',
        key: 'cabang_id'
      }
    },
    cabang_start: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    cabang_end: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    divisi_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    jabatan_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    lvljabatan_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    lvl2jabatan_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    divisi_jabatan_start: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    divisi_jabatan_estend: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    cetak_kartu: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0'
    },
    tdate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    rec_status: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '0'
    },
    status_pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    status_pegawai_start: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    status_pegawai_estend: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    hubungan_kerja_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    hubungan_kerja_start: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    hubungan_kerja_estend: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    parent_update_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    binding_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    pegawai_sync: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    pegawai_imp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    no_surat: {
      type: DataTypes.STRING(250),
      allowNull: true
    }
  }, {
    tableName: 'pegawai'
  });


  Model.getPersonId = function (pegawai_id,fn) {
    sequelize.models.status.findOne({where: {nama_status: 'KARYAWAN'}})
        .then(stat => {
          if (stat) {
            sequelize.models.family_pegawai.findOne({
              where: {
                pegawai_id: pegawai_id,
                status_id: stat.status_id
              }
            }).then(fampeg => {
              fn(fampeg.person_id);
            });
          }
        });


  };

  return Model;
}
