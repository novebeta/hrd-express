/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trans', {
    trans_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    doc_ref: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    tgl: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    tdate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    bruto: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    disc: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    disc_rp: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ket_disc: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    vat: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    total_pot: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    total: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    total_disc_rp: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    tipe: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    no_faktur: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    user_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    total_disc_rp_1: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'pegawai',
        key: 'pegawai_id'
      }
    },
    cabang_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'cabang',
        key: 'cabang_id'
      }
    },
    family_pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'family_pegawai',
        key: 'family_pegawai_id'
      }
    },
    info: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'trans'
  });
};
