/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('bu', {
    bu_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    bu_name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    bu_nama_alias: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    bu_kode: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    pt_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    last_kode: {
      type: DataTypes.STRING(10),
      allowNull: false,
      defaultValue: '100001'
    },
    tgl_berdiri: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    jenis_usaha: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    direktur: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    almt: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    profile: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    private: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'bu'
  });
};
