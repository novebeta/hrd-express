/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cabang', {
    cabang_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    kode_cabang: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    nama_cabang: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    bu_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'bu',
        key: 'bu_id'
      }
    },
    alamat_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    no_telp_cabang: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    alamat_cabang: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    kepala_cabang_stat: {
      type: DataTypes.INTEGER(2),
      allowNull: true,
      defaultValue: '1'
    },
    area_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    }
  }, {
    tableName: 'cabang'
  });
};
