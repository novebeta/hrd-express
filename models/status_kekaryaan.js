/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('status_kekaryaan', {
    status_kekaryaan_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    kode_status_kekaryaan: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    nama_status_kekaryaan: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'status_kekaryaan'
  });
};
