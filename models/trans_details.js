/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trans_details', {
    trans_details_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    item: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    qty: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    price: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    total_line: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    disc: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    disc_rp: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ket_pot: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    vat: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    vat_rp: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    bruto: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    total_pot: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    disc_name: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    disc_1: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    disc_rp_1: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    trans_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'trans',
        key: 'trans_id'
      }
    }
  }, {
    tableName: 'trans_details'
  });
};
