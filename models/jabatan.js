/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('jabatan', {
    jabatan_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    kode_jabatan: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    nama_jabatan: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    bu_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      references: {
        model: 'bu',
        key: 'bu_id'
      }
    },
    urutan: {
      type: DataTypes.INTEGER(3),
      allowNull: true,
      defaultValue: '0'
    }
  }, {
    tableName: 'jabatan'
  });
};
