/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hr_cuti', {
    hr_cuti_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    jatah_cuti: {
      type: DataTypes.INTEGER(2),
      allowNull: true
    },
    tahun: {
      type: DataTypes.INTEGER(10),
      allowNull: true
    }
  }, {
    tableName: 'hr_cuti'
  });
};
