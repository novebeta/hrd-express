/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pbu_pegawai', {
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    nik: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    nama_lengkap: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    tgl_masuk: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    npwp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    bank_nama: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    bank_kota: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    rekening: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    tdate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    last_update: {
      type: DataTypes.DATE,
      allowNull: false
    },
    last_update_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    status_pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    jabatan_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    tuser: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    leveling_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    cabang_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    status_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    golongan_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    store: {
      type: DataTypes.STRING(20),
      allowNull: false
    }
  }, {
    tableName: 'pbu_pegawai'
  });
};
