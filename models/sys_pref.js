/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sys_pref', {
    menu: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    value: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    sys_pref_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      defaultValue: '',
      primaryKey: true
    }
  }, {
    tableName: 'sys_pref'
  });
};
