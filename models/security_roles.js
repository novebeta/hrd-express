/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('security_roles', {
    security_roles_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    role: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    ket: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    sections: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'security_roles'
  });
};
