/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('approval', {
    approval_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    type_: {
      type: DataTypes.STRING(5),
      allowNull: false
    },
    trans_no: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    level_: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0'
    },
    tdate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    user_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    },
    approval_user_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    }
  }, {
    tableName: 'approval'
  });
};
