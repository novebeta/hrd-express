/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('kota', {
    kota_id: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      primaryKey: true
    },
    nama_kota: {
      type: DataTypes.STRING(36),
      allowNull: true
    }
  }, {
    tableName: 'kota'
  });
};
