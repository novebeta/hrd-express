/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('faq_kredit', {
    faq_kredit_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    title: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    publish_date: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    published: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: '0'
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    tdate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    user_id: {
      type: DataTypes.STRING(36),
      allowNull: false
    }
  }, {
    tableName: 'faq_kredit'
  });
};
