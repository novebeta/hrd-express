/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ptkp', {
    ptkp_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    ptkp_kode: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    ptkp_nama: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    urutan: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'ptkp'
  });
};
