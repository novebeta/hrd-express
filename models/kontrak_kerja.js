/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('kontrak_kerja', {
    kontrak_id: {
      type: DataTypes.STRING(36),
      allowNull: false,
      primaryKey: true
    },
    kontrak_from: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    kontrak_to: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    ket: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    pegawai_id: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    rec_status: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: '1'
    }
  }, {
    tableName: 'kontrak_kerja'
  });
};
