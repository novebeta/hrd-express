var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var areaRouter = require('./routes/area');
var bankRouter = require('./routes/bank');
var buRouter = require('./routes/bu');
var cabangRouter = require('./routes/cabang');
var golonganRouter = require('./routes/golongan');
var jabatanRouter = require('./routes/jabatan');
var levelingRouter = require('./routes/leveling');
var pegawaiRouter = require('./routes/pegawai');
var pegawaibankRouter = require('./routes/pegawaibank');
var statusRouter = require('./routes/status');
var statuspegawaiRouter = require('./routes/statuspegawai');
var usersRouter = require('./routes/users');
var divisiRouter = require('./routes/divisi');

var app = express();

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(function(req, res, next) {
  if (!'authorization' in req.headers) {
    return res.status(403).json({ error: 'No credentials sent!' });
  }
  if (req.headers.authorization !== "B7076B5F-269E-11E8-8341-6DB33A098066") {
    return res.status(403).json({ error: 'Wrong credentials!!!' });
  }
  next();
});

app.use('/', indexRouter);
app.use('/area', areaRouter);
app.use('/bank', bankRouter);
app.use('/bu', buRouter);
app.use('/cabang', cabangRouter);
app.use('/golongan', golonganRouter);
app.use('/jabatan', jabatanRouter);
app.use('/leveling', levelingRouter);
app.use('/pegawai', pegawaiRouter);
app.use('/pegawaibank', pegawaibankRouter);
app.use('/status', statusRouter);
app.use('/statuspegawai', statuspegawaiRouter);
app.use('/users', usersRouter);
app.use('/divisi', divisiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
